﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace csharp_selenium_facebook.libs.AutoTest
{
    public class FacebookAutoTest
    {
        private String UserName { get; set; }
        private String Password { get; set; }
        private IWebDriver driver = new ChromeDriver();
        const String searc = "Test"; 

        public FacebookAutoTest(String username, String password)
        {
            UserName = username;
            Password = password;
        }

        //Login to facebook
        private void Login() {
            driver.Navigate().GoToUrl("http://www.facebook.com/");
            //Login input
            var login_element = driver.FindElement(By.XPath("//*[@id='email']"));
            login_element.SendKeys(UserName);
            //Password input
            var password_element = driver.FindElement(By.XPath("//*[@id='pass']"));
            password_element.SendKeys(Password);
            driver.FindElement(By.XPath("//*[@id='login_form']/table/tbody/tr[2]/td[3]")).Click();
            //Close modal window
            driver.FindElement(By.LinkText("Not Now")).Click();
        }

        //Send to searc field "Test" text 
        private void FindTest() {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            var query = driver.FindElement(By.Name("q"));
            query.SendKeys(searc); 
            query.SendKeys(Keys.Return);
        }

        public void Run(){
            Login();
            FindTest();  
        }
    }
}
