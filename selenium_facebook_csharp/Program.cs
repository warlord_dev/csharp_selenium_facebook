﻿using csharp_selenium_facebook.libs.AutoTest;
using System.Configuration;

namespace selenium_facebook_csharp

{
    class MainClass
    {
        static string login = ConfigurationSettings.AppSettings["login"];
        static string password = ConfigurationSettings.AppSettings["password"];

        static void Main(string[] args)
        {
            var test = new FacebookAutoTest(login, password);
            test.Run();
        }
    }
}
